package ru.music.example.dto.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class PromotionRecordingRequestDto {

    @NotNull
    private int idRecording;
    @Size(min = 5, message = "name should at most 5 characters long")
    private String name;
    private BigDecimal money;
    private int days;

    public int getIdRecording() {
        return idRecording;
    }

    public void setIdRecording(int idRecording) {
        this.idRecording = idRecording;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }
}
