package ru.music.example.dto.request;

import javax.validation.constraints.Size;

public class PublishRecordingRequestDto {

    @Size(min = 5, message = "Channel name should at most 5 characters long")
    private String channelName;
    private int recordingId;
    private int days;

    public PublishRecordingRequestDto() {
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getRecordingId() {
        return recordingId;
    }

    public void setRecordingId(int recordingId) {
        this.recordingId = recordingId;
    }
}
