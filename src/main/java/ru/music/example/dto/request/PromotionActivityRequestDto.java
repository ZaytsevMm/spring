package ru.music.example.dto.request;

import javax.validation.constraints.NotNull;

public class PromotionActivityRequestDto {

    @NotNull
    private boolean activity;

    public PromotionActivityRequestDto() {
    }

    public boolean isActivity() {
        return activity;
    }

    public void setActivity(boolean activity) {
        this.activity = activity;
    }
}
