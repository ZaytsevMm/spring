package ru.music.example.dto.response;

public class RecordingResponseDto {

    private int id;

    public RecordingResponseDto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
