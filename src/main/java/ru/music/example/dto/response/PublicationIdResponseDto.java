package ru.music.example.dto.response;


public class PublicationIdResponseDto {

    private int id;

    public PublicationIdResponseDto(){

    }

    public PublicationIdResponseDto(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
