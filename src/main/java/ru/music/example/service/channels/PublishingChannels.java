package ru.music.example.service.channels;

import ru.music.example.model.Publication;

public interface PublishingChannels {

    int publish(Publication publication);

}
