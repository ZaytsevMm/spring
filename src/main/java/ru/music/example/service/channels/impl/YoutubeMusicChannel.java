package ru.music.example.service.channels.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.music.example.model.Publication;
import ru.music.example.service.channels.PublishingChannels;
import ru.music.example.storage.ChannelsRecordingStorage;

@Service("youtubeMusicChannel")
public class YoutubeMusicChannel implements PublishingChannels {


    private ChannelsRecordingStorage channelsRecordingStorage;

    @Autowired
    public YoutubeMusicChannel(ChannelsRecordingStorage channelsRecordingStorage) {
        this.channelsRecordingStorage = channelsRecordingStorage;
    }

    @Override
    public int publish(Publication publication) {
        System.out.println("Publish in YoutubeMusicChannel " + publication);
        return channelsRecordingStorage.savePublication(publication);
    }

}
