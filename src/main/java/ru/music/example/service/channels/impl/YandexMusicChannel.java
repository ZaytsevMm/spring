package ru.music.example.service.channels.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.music.example.model.Publication;
import ru.music.example.service.channels.PublishingChannels;
import ru.music.example.storage.ChannelsRecordingStorage;

@Service("yandexMusicChannel")
public class YandexMusicChannel implements PublishingChannels {


    private ChannelsRecordingStorage channelsRecordingStorage;

    @Autowired
    public YandexMusicChannel(ChannelsRecordingStorage channelsRecordingStorage) {
        this.channelsRecordingStorage = channelsRecordingStorage;
    }

    @Override
    public int publish(Publication publication) {
        System.out.println("Publish in YandexMusicChannel " + publication);
        return channelsRecordingStorage.savePublication(publication);
    }
}
