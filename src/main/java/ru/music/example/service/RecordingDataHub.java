package ru.music.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.music.example.model.Recording;
import ru.music.example.service.exception.ErrorNotFound;
import ru.music.example.storage.AudioRecordingStorage;
import ru.music.example.storage.VideoStorage;

import java.util.HashMap;
import java.util.Map;

@Service
public class RecordingDataHub {

    private AudioRecordingStorage audioRecordingStorage;
    private VideoStorage videoStorage;
    private Map<Integer, Recording> recordings = new HashMap<>();

    @Autowired
    public RecordingDataHub(AudioRecordingStorage audioRecordingStorage, VideoStorage videoStorage) {
        this.audioRecordingStorage = audioRecordingStorage;
        this.videoStorage = videoStorage;
    }

    public String saveAudio(String path) {
        return audioRecordingStorage.save(path);
    }

    public String saveVideo(String path) {
        return videoStorage.save(path);
    }

    public int saveRecording(Recording recording) {
        recording.setLinkAudio(saveAudio(recording.getLinkAudio()));
        recording.setLinkVideo(saveVideo(recording.getLinkVideo()));
        int id = recordings.size() + 1;
        recordings.put(id, recording);
        return id;
    }

    public Recording getRecording(int id) throws ErrorNotFound {
        Recording recording = recordings.get(id);
        if (recording == null) {
            throw new ErrorNotFound(Recording.class.getSimpleName(), String.valueOf(id));
        }
        return recording;
    }
}

