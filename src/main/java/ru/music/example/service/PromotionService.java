package ru.music.example.service;

import org.springframework.stereotype.Service;
import ru.music.example.model.Promotion;
import ru.music.example.model.Recording;
import ru.music.example.service.exception.ErrorNotFound;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.*;

@Service
public class PromotionService {

    private Map<String, Promotion> promotions = new HashMap<>();

    public void createPromotion(String name, BigDecimal money, Recording recording, ZonedDateTime dt) {
        System.out.println("Create promotion.....");
        if (!promotions.containsKey(name)) {
            promotions.put(name, new Promotion(money, recording));
        }
    }

    public void setActivity(String name, boolean activity) throws ErrorNotFound {
        System.out.println("Set activity promotion.....");
        if (promotions.containsKey(name)) {
            Promotion promotion = promotions.get(name);
            promotion.setActivity(activity);
        } else {
            throw new ErrorNotFound(Promotion.class.getSimpleName(), name);
        }
    }

    public void deletePromotion(String name) throws ErrorNotFound {
        System.out.println("Delete promotion.....");
        if (promotions.containsKey(name)) {
            promotions.remove(name);
        } else {
            throw new ErrorNotFound(Promotion.class.getSimpleName(), name);
        }
    }

}
