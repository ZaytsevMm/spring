package ru.music.example.service.exception;

public class ErrorNotFound extends Exception {
    public ErrorNotFound(String entity, String param) {
        super(String.format("%s %s not found", entity, param));
    }
}
