package ru.music.example.service.exception;

import java.util.ArrayList;
import java.util.List;

public class ErrorResponse {

    private List<String> allErrors = new ArrayList<>();

    public List<String> getAllErrors() {
        return allErrors;
    }

    public void setAllErrors(List<String> allErrors) {
        this.allErrors = allErrors;
    }
}
