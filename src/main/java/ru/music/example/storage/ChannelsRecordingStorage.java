package ru.music.example.storage;

import org.springframework.stereotype.Repository;
import ru.music.example.model.Publication;

import java.util.HashMap;
import java.util.Map;

@Repository
public class ChannelsRecordingStorage {

    private Map<Integer, Publication> publications = new HashMap<>();

    public int savePublication(Publication publication) {
        Integer id = publications.size() + 1;
        publications.put(id, publication);
        return id;
    }

    public void deletePublication(int id) {
        publications.remove(id);
    }
}
