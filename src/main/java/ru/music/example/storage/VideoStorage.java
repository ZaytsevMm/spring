package ru.music.example.storage;

import org.springframework.stereotype.Repository;

@Repository
public class VideoStorage implements DataStorage {

    @Override
    public String save(String path) {
        if (path == null || path.equals("")) {
            return null;
        }
        System.out.println("Save video to 3gp");
        return path + ".3gp";
    }

}
