package ru.music.example.storage;


import org.springframework.stereotype.Repository;

@Repository
public class AudioRecordingStorage implements DataStorage {

    @Override
    public String save(String path) {
        if (path == null || path.equals("")) {
            return null;
        }
        System.out.println("Save audio to mp3");
        return path + ".mp3";
    }

}
