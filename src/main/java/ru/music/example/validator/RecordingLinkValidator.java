package ru.music.example.validator;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RecordingLinkValidator implements ConstraintValidator<RecordingLink, Object> {

    @Override
    public void initialize(RecordingLink constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext constraintValidatorContext) {
        String linkVideo = (String) new BeanWrapperImpl(obj)
                .getPropertyValue("linkVideo");
        String linkAudio = (String) new BeanWrapperImpl(obj)
                .getPropertyValue("linkAudio");
        if (linkVideo == null && linkAudio == null) {
            return false;
        } else if (linkVideo == null && "" .equals(linkAudio)) {
            return false;
        } else if ("" .equals(linkVideo) && linkAudio == null) {
            return false;
        } else if (linkVideo.equals("") && linkAudio.equals("")) {
            return false;
        }
        return true;
    }
}
