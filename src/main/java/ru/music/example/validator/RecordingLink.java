package ru.music.example.validator;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = RecordingLinkValidator.class)
@Target( { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface RecordingLink {
    String message() default "The recording must have a link to audio or video";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}