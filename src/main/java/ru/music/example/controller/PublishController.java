package ru.music.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.music.example.dto.response.PublicationIdResponseDto;
import ru.music.example.dto.request.PublishRecordingRequestDto;
import ru.music.example.dto.response.EmptyJsonResponseDto;
import ru.music.example.model.Publication;
import ru.music.example.model.Recording;
import ru.music.example.service.RecordingDataHub;
import ru.music.example.service.channels.PublishingChannels;
import ru.music.example.service.exception.ErrorNotFound;
import ru.music.example.storage.ChannelsRecordingStorage;

import java.time.ZonedDateTime;

@RestController
@RequestMapping("/api/publications")
public class PublishController {

    private PublishingChannels publishingItunes;
    private PublishingChannels publishingYandex;
    private PublishingChannels publishingYoutube;
    private RecordingDataHub recordingDataHub;
    private ChannelsRecordingStorage channelsRecordingStorage;

    @Autowired
    public PublishController(@Qualifier("itunesChannel") PublishingChannels publishingItunes,
                             @Qualifier("yandexMusicChannel") PublishingChannels publishingYandex,
                             @Qualifier("youtubeMusicChannel") PublishingChannels publishingYoutube,
                             RecordingDataHub recordingDataHub, ChannelsRecordingStorage channelsRecordingStorage) {
        this.publishingItunes = publishingItunes;
        this.publishingYandex = publishingYandex;
        this.publishingYoutube = publishingYoutube;
        this.recordingDataHub = recordingDataHub;
        this.channelsRecordingStorage = channelsRecordingStorage;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public PublicationIdResponseDto publishRecording(@RequestBody PublishRecordingRequestDto dtoRequest)
            throws ErrorNotFound {
        Recording recording = recordingDataHub.getRecording(dtoRequest.getRecordingId());
        Publication publication = new Publication(dtoRequest.getChannelName(), recording,
                ZonedDateTime.now().plusDays(dtoRequest.getDays()));
        switch (dtoRequest.getChannelName()) {
            case ("itunes"):
                return new PublicationIdResponseDto(publishingItunes.publish(publication));
            case ("youtube"):
                return new PublicationIdResponseDto(publishingYoutube.publish(publication));
            case ("yandex"):
                return new PublicationIdResponseDto(publishingYandex.publish(publication));
            default:
                throw new ErrorNotFound("Chanel", dtoRequest.getChannelName());
        }
    }

    @DeleteMapping(value = "/{publicationId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public EmptyJsonResponseDto deletePublication(@PathVariable("publicationId") int publicationId) {
        channelsRecordingStorage.deletePublication(publicationId);
        return new EmptyJsonResponseDto();
    }
}
