package ru.music.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.music.example.dto.response.RecordingResponseDto;
import ru.music.example.model.Recording;
import ru.music.example.service.RecordingDataHub;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/recordings")
public class RecordingController {

    private RecordingDataHub recordingDataHub;

    @Autowired
    public RecordingController(RecordingDataHub recordingDataHub) {
        this.recordingDataHub = recordingDataHub;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public RecordingResponseDto saveRecording(@Valid @RequestBody Recording recording) {
        RecordingResponseDto dtoResponse = new RecordingResponseDto();
        dtoResponse.setId(recordingDataHub.saveRecording(recording));
        return dtoResponse;
    }
}
