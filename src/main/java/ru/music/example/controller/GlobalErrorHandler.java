package ru.music.example.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import ru.music.example.service.exception.ErrorNotFound;
import ru.music.example.service.exception.ErrorResponse;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class GlobalErrorHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse handleValidation(MethodArgumentNotValidException exc) {
        ErrorResponse error = new ErrorResponse();
        exc.getBindingResult().getAllErrors().forEach(fieldError -> {
            error.getAllErrors().add(fieldError.getDefaultMessage());
        });
        return error;
    }

    @ExceptionHandler(ErrorNotFound.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse exceptionNotFound(ErrorNotFound exc) {
        ErrorResponse error = new ErrorResponse();
        error.getAllErrors().add(exc.getMessage());
        return error;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse exceptionConstraintViolation(ConstraintViolationException exc) {
        ErrorResponse error = new ErrorResponse();
        error.getAllErrors().add(exc.getMessage());
        return error;
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse exceptionMethodArgumentMissRequestParameter(MissingServletRequestParameterException exc) {
        final ErrorResponse error = new ErrorResponse();
        error.getAllErrors().add(exc.getMessage());
        return error;
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse exceptionMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException exc) {
        ErrorResponse error = new ErrorResponse();
        error.getAllErrors().add(exc.getMessage());
        return error;
    }
}
