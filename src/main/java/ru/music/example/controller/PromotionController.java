package ru.music.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.music.example.dto.request.PromotionRecordingRequestDto;
import ru.music.example.dto.request.PromotionActivityRequestDto;
import ru.music.example.dto.response.EmptyJsonResponseDto;
import ru.music.example.model.Recording;
import ru.music.example.service.PromotionService;
import ru.music.example.service.RecordingDataHub;
import ru.music.example.service.exception.ErrorNotFound;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

@RestController
@RequestMapping("/api/promotions")
@Validated
public class PromotionController {

    private PromotionService promotionService;
    private RecordingDataHub recordingDataHub;

    @Autowired
    public PromotionController(PromotionService promotionService, RecordingDataHub recordingDataHub) {
        this.promotionService = promotionService;
        this.recordingDataHub = recordingDataHub;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public EmptyJsonResponseDto createPromotion(@Valid @RequestBody PromotionRecordingRequestDto dtoRequest)
            throws ErrorNotFound {
        Recording recording = recordingDataHub.getRecording(dtoRequest.getIdRecording());
        promotionService.createPromotion(dtoRequest.getName(), dtoRequest.getMoney(), recording,
                ZonedDateTime.now().plusDays(dtoRequest.getDays()));
        return new EmptyJsonResponseDto();
    }

    @PutMapping(value = "/{namePromotion}", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public EmptyJsonResponseDto setActivityPromotion(
            @RequestBody PromotionActivityRequestDto dtoRequest,
            @Size(min = 5, message = "name should at most 5 characters long")
            @PathVariable("namePromotion") String name) throws ErrorNotFound {
        promotionService.setActivity(name, dtoRequest.isActivity());
        return new EmptyJsonResponseDto();
    }

    @DeleteMapping(value = "/{namePromotion}", produces = MediaType.APPLICATION_JSON_VALUE)
    public EmptyJsonResponseDto deletePromotion(
            @Size(min = 5, message = "name should at most 5 characters long")
            @PathVariable("namePromotion") String name) throws ErrorNotFound {
        promotionService.deletePromotion(name);
        return new EmptyJsonResponseDto();
    }

}
