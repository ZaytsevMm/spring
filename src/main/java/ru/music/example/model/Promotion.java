package ru.music.example.model;

import java.math.BigDecimal;

public class Promotion {

    private BigDecimal money;
    private Recording recording;
    private boolean activity;

    public Promotion(BigDecimal money, Recording recording) {
        this.money = money;
        this.recording = recording;
        this.activity = true;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Recording getRecording() {
        return recording;
    }

    public void setRecording(Recording recording) {
        this.recording = recording;
    }

    public boolean isActivity() {
        return activity;
    }

    public void setActivity(boolean activity) {
        this.activity = activity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Promotion)) return false;

        Promotion promotion = (Promotion) o;

        if (isActivity() != promotion.isActivity()) return false;
        if (getMoney() != null ? !getMoney().equals(promotion.getMoney()) : promotion.getMoney() != null) return false;
        return getRecording() != null ? getRecording().equals(promotion.getRecording()) : promotion.getRecording() == null;
    }

    @Override
    public int hashCode() {
        int result = getMoney() != null ? getMoney().hashCode() : 0;
        result = 31 * result + (getRecording() != null ? getRecording().hashCode() : 0);
        result = 31 * result + (isActivity() ? 1 : 0);
        return result;
    }
}
