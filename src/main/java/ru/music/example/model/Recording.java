package ru.music.example.model;

import ru.music.example.validator.RecordingLink;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@RecordingLink
public class Recording {

    @NotBlank(message = "The artist should be not null")
    private String artist;
    @NotBlank (message = "The name composition should be not null")
    private String nameComposition;
    private String album;
    private String linkPictureAlbum;
    @Min(value = 1971, message = " year > 1970")
    private int year;
    @NotBlank(message = "The genre should be not null")
    private String genre;
    @NotBlank(message = "The time should be not null")
    private String time;
    private String linkAudio;
    private String linkVideo;

    public Recording() {
    }

    public Recording(String artist, String nameComposition, String album, String linkPictureAlbum, int year,
                     String genre, String time, String linkAudio, String linkVideo) {
        this.artist = artist;
        this.nameComposition = nameComposition;
        this.album = album;
        this.linkPictureAlbum = linkPictureAlbum;
        this.year = year;
        this.genre = genre;
        this.time = time;
        this.linkAudio = linkAudio;
        this.linkVideo = linkVideo;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getNameComposition() {
        return nameComposition;
    }

    public void setNameComposition(String nameComposition) {
        this.nameComposition = nameComposition;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getLinkPictureAlbum() {
        return linkPictureAlbum;
    }

    public void setLinkPictureAlbum(String linkPictureAlbum) {
        this.linkPictureAlbum = linkPictureAlbum;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLinkAudio() {
        return linkAudio;
    }

    public void setLinkAudio(String linkAudio) {
        this.linkAudio = linkAudio;
    }

    public String getLinkVideo() {
        return linkVideo;
    }

    public void setLinkVideo(String linkVideo) {
        this.linkVideo = linkVideo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Recording)) return false;

        Recording recording = (Recording) o;

        if (getArtist() != null ? !getArtist().equals(recording.getArtist()) : recording.getArtist() != null)
            return false;
        if (getNameComposition() != null ? !getNameComposition().equals(recording.getNameComposition()) : recording.getNameComposition() != null)
            return false;
        if (getAlbum() != null ? !getAlbum().equals(recording.getAlbum()) : recording.getAlbum() != null) return false;
        if (getLinkPictureAlbum() != null ? !getLinkPictureAlbum().equals(recording.getLinkPictureAlbum()) : recording.getLinkPictureAlbum() != null)
            return false;
        if (getGenre() != null ? !getGenre().equals(recording.getGenre()) : recording.getGenre() != null) return false;
        if (getTime() != null ? !getTime().equals(recording.getTime()) : recording.getTime() != null) return false;
        if (getLinkAudio() != null ? !getLinkAudio().equals(recording.getLinkAudio()) : recording.getLinkAudio() != null)
            return false;
        return getLinkVideo() != null ? getLinkVideo().equals(recording.getLinkVideo()) : recording.getLinkVideo() == null;
    }

    @Override
    public int hashCode() {
        int result = getArtist() != null ? getArtist().hashCode() : 0;
        result = 31 * result + (getNameComposition() != null ? getNameComposition().hashCode() : 0);
        result = 31 * result + (getAlbum() != null ? getAlbum().hashCode() : 0);
        result = 31 * result + (getLinkPictureAlbum() != null ? getLinkPictureAlbum().hashCode() : 0);
        result = 31 * result + (getGenre() != null ? getGenre().hashCode() : 0);
        result = 31 * result + (getTime() != null ? getTime().hashCode() : 0);
        result = 31 * result + (getLinkAudio() != null ? getLinkAudio().hashCode() : 0);
        result = 31 * result + (getLinkVideo() != null ? getLinkVideo().hashCode() : 0);
        return result;
    }
}
