package ru.music.example.model;

import java.time.ZonedDateTime;

public class Publication {

    private String channelName;
    private Recording recording;
    private ZonedDateTime dt;

    public Publication(String channelName, Recording recording, ZonedDateTime dt) {
        this.channelName = channelName;
        this.recording = recording;
        this.dt = dt;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Recording getRecording() {
        return recording;
    }

    public void setRecording(Recording recording) {
        this.recording = recording;
    }

    public ZonedDateTime getDt() {
        return dt;
    }

    public void setDt(ZonedDateTime dt) {
        this.dt = dt;
    }
}
