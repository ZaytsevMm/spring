package ru.music.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import ru.music.example.dto.request.PromotionRecordingRequestDto;
import ru.music.example.dto.request.PromotionActivityRequestDto;
import ru.music.example.dto.request.PublishRecordingRequestDto;
import ru.music.example.dto.response.PublicationIdResponseDto;
import ru.music.example.dto.response.RecordingResponseDto;
import ru.music.example.model.Recording;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TestApplication {

    private Recording recording;

    @BeforeEach
    public void setUp() {
        recording = new Recording("kirkorov", "blue", "winter",
                null, 2018, "pop", "3:80", "www.audio.ru/blue",
                "www.video.ru/blue");
    }

    @Test
    public void testApplication() {
        //save recording
        RecordingResponseDto dtoResponse = WebTestClient
                .bindToServer()
                .baseUrl("http://localhost:8080")
                .build()
                .post()
                .uri("/api/recordings")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(recording)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .returnResult(RecordingResponseDto.class).getResponseBody().blockFirst();
        assertEquals(1, dtoResponse.getId());

        //publish in itunes
        PublishRecordingRequestDto itunesDto = new PublishRecordingRequestDto();
        itunesDto.setChannelName("itunes");
        itunesDto.setRecordingId(dtoResponse.getId());
        itunesDto.setDays(0);
        PublicationIdResponseDto resultItunesPublish = WebTestClient
                .bindToServer()
                .baseUrl("http://localhost:8080")
                .build()
                .post()
                .uri("/api/publications")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(itunesDto)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .returnResult(PublicationIdResponseDto.class).getResponseBody().blockFirst();
        assertEquals(1, resultItunesPublish.getId());

        //publish in youtube
        PublishRecordingRequestDto youtubeDto = new PublishRecordingRequestDto();
        youtubeDto.setChannelName("youtube");
        youtubeDto.setRecordingId(dtoResponse.getId());
        youtubeDto.setDays(1);
        PublicationIdResponseDto resultYoutubePublish = WebTestClient
                .bindToServer()
                .baseUrl("http://localhost:8080")
                .build()
                .post()
                .uri("/api/publications")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(youtubeDto)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .returnResult(PublicationIdResponseDto.class).getResponseBody().blockFirst();
        assertEquals(2, resultYoutubePublish.getId());

        //delete publish in youtube
        Flux<String> resultDeleteYoutubePublish = WebTestClient
                .bindToServer()
                .baseUrl("http://localhost:8080")
                .build()
                .delete()
                .uri("/api/publications/" + resultYoutubePublish.getId())
                .exchange()
                .expectStatus().isOk()
                .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .returnResult(String.class).getResponseBody();
        assertEquals("{}", resultDeleteYoutubePublish.blockFirst());

        //create promotion
        PromotionRecordingRequestDto promotionRecordingRequestDto = new PromotionRecordingRequestDto();
        promotionRecordingRequestDto.setIdRecording(dtoResponse.getId());
        promotionRecordingRequestDto.setName("promo");
        promotionRecordingRequestDto.setMoney(new BigDecimal(132.10));
        Flux<String> resultCreatePromotion = WebTestClient
                .bindToServer()
                .baseUrl("http://localhost:8080")
                .build()
                .post()
                .uri("/api/promotions")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(promotionRecordingRequestDto)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .returnResult(String.class).getResponseBody();
        assertEquals("{}", resultCreatePromotion.blockFirst());

        //set activity promotion
        PromotionActivityRequestDto promotionActivityRequestDto = new PromotionActivityRequestDto();
        promotionActivityRequestDto.setActivity(false);
        Flux<String> resultStopPromotion = WebTestClient
                .bindToServer()
                .baseUrl("http://localhost:8080")
                .build()
                .put()
                .uri("/api/promotions/promo")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(promotionActivityRequestDto)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .returnResult(String.class).getResponseBody();
        assertEquals("{}", resultStopPromotion.blockFirst());

        //delete promotion
        Flux<String> resultDeletePromotion = WebTestClient
                .bindToServer()
                .baseUrl("http://localhost:8080")
                .build()
                .delete()
                .uri("/api/promotions/promo")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .returnResult(String.class).getResponseBody();
        assertEquals("{}", resultDeletePromotion.blockFirst());
    }
}
