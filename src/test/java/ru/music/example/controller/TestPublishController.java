package ru.music.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.music.example.dto.request.PublishRecordingRequestDto;
import ru.music.example.dto.response.PublicationIdResponseDto;
import ru.music.example.model.Publication;
import ru.music.example.model.Recording;
import ru.music.example.service.RecordingDataHub;
import ru.music.example.service.channels.PublishingChannels;
import ru.music.example.service.exception.ErrorNotFound;
import ru.music.example.service.exception.ErrorResponse;
import ru.music.example.storage.ChannelsRecordingStorage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PublishController.class)
public class TestPublishController {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    @Qualifier(value = "itunesChannel")
    private PublishingChannels publishingItunes;
    @MockBean
    @Qualifier(value = "yandexMusicChannel")
    private PublishingChannels publishingYandex;
    @MockBean
    @Qualifier(value = "youtubeMusicChannel")
    private PublishingChannels publishingYoutube;
    @MockBean
    private RecordingDataHub recordingDataHub;
    @MockBean
    private ChannelsRecordingStorage channelsRecordingStorage;

    private Recording recording;

    @BeforeEach
    public void setUp() throws ErrorNotFound {
        recording = new Recording("kirkorov", "blue", "winter",
                null, 2018, "pop", "3:80", "www.audio.ru/blue",
                "www.video.ru/blue");
        when(recordingDataHub.getRecording(1)).thenReturn(recording);
        when(recordingDataHub.getRecording(12)).thenThrow(
                new ErrorNotFound(Recording.class.getSimpleName(), String.valueOf(12)));
        when(publishingYoutube.publish(any(Publication.class))).thenReturn(1);
        when(publishingYandex.publish(any(Publication.class))).thenReturn(1);
        when(publishingItunes.publish(any(Publication.class))).thenReturn(1);
    }

    @Test
    public void testPublishRecording() throws Exception {
        PublishRecordingRequestDto dto = new PublishRecordingRequestDto();
        dto.setChannelName("youtube");
        dto.setRecordingId(1);
        dto.setDays(0);
        MvcResult result = mvc.perform(post("/api/publications")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(dto)))
                .andReturn();
        PublicationIdResponseDto dtoResponse = mapper.reader().forType(PublicationIdResponseDto.class).
                readValue(result.getResponse().getContentAsString());
        assertEquals(1, dtoResponse.getId());
    }

    @Test
    public void testPublishRecordingBadRequest() throws Exception {
        PublishRecordingRequestDto dto = new PublishRecordingRequestDto();
        dto.setChannelName("youtube");
        dto.setRecordingId(12);
        dto.setDays(0);
        MvcResult result = mvc.perform(post("/api/publications")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(dto)))
                .andReturn();
        ErrorResponse error = mapper.readValue(result.getResponse().getContentAsString(),
                ErrorResponse.class);
        assertEquals(1, error.getAllErrors().size());
    }

    @Test
    public void testDeletePublishRecording() throws Exception {
        MvcResult result = mvc.perform(delete("/api/publications/1"))
                .andReturn();
        assertEquals("{}", result.getResponse().getContentAsString());
        assertEquals(result.getResponse().getStatus(), 200);
    }
}
