package ru.music.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.music.example.model.Recording;
import ru.music.example.service.RecordingDataHub;
import ru.music.example.service.exception.ErrorResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = RecordingController.class)
public class TestRecordingController {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private RecordingDataHub recordingDataHub;

    @Test
    public void testAddRecording() throws Exception {
        Mockito.when(recordingDataHub.saveRecording(Mockito.any(Recording.class))).thenReturn(1);
        Recording recording = new Recording("kirkorov", "blue", "winter",
                null, 2018, "pop", "3:80", "www.audio.ru/blue",
                "www.video.ru/blue");
        MvcResult result = mvc.perform(post("/api/recordings")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(recording)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 200);
    }

    @Test
    public void testValidatorRecording() throws Exception {
        Recording recording = new Recording(null, null, "winter",
                null, 1970, "pop", "3:80", null,
                "");
        MvcResult result = mvc.perform(post("/api/recordings")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(recording)))
                .andReturn();
        ErrorResponse error = mapper.readValue(result.getResponse().getContentAsString(),
                ErrorResponse.class);
        assertEquals(4, error.getAllErrors().size());
    }
}
