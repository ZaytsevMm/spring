package ru.music.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.music.example.dto.request.PromotionRecordingRequestDto;
import ru.music.example.dto.request.PromotionActivityRequestDto;
import ru.music.example.model.Recording;
import ru.music.example.service.PromotionService;
import ru.music.example.service.RecordingDataHub;
import ru.music.example.service.exception.ErrorNotFound;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PromotionController.class)
public class TestPromotionController {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private PromotionService promotionService;
    @MockBean
    private RecordingDataHub recordingDataHub;

    private Recording recording;

    @BeforeEach
    public void setUp() throws ErrorNotFound {
        recording = new Recording("kirkorov", "blue", "winter",
                null, 2018, "pop", "3:80", "www.audio.ru/blue",
                "www.video.ru/blue");
        when(recordingDataHub.getRecording(1)).thenReturn(recording);
    }

    @Test
    public void testCreatePromotion() throws Exception {
        PromotionRecordingRequestDto dto = new PromotionRecordingRequestDto();
        dto.setIdRecording(1);
        dto.setName("promo");
        dto.setMoney(new BigDecimal(132.10));
        dto.setDays(0);
        MvcResult result = mvc.perform(post("/api/promotions")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(dto)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 200);
        assertEquals("{}", result.getResponse().getContentAsString());
    }

    @Test
    public void testCreatePromotionBadRequest() throws Exception {
        PromotionRecordingRequestDto dto = new PromotionRecordingRequestDto();
        dto.setIdRecording(1);
        dto.setName("p"); //name < 5
        dto.setMoney(new BigDecimal(132.10));
        dto.setDays(0);
        MvcResult result = mvc.perform(post("/api/promotions")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(dto)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void testStartStopPromotion() throws Exception {
        PromotionActivityRequestDto dto = new PromotionActivityRequestDto();
        dto.setActivity(false);
        MvcResult result = mvc.perform(put("/api/promotions/promo")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(dto)))
                .andReturn();
        assertEquals("{}", result.getResponse().getContentAsString());
        assertEquals(result.getResponse().getStatus(), 200);
    }

    @Test
    public void testDeletePromotion() throws Exception {
        MvcResult result = mvc.perform(delete("/api/promotions/promo"))
                .andReturn();
        assertEquals("{}", result.getResponse().getContentAsString());
        assertEquals(result.getResponse().getStatus(), 200);
    }
}
